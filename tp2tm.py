#!/usr/bin/python
# -*- coding: utf-8 -*-

from sys import argv, exit
from os import listdir
from os.path import join, getsize
from copy import deepcopy
from base64 import b64decode
import logging

try:
    from lxml import etree
except ImportError:
    exit("Please install the python-lxml library.")

minlevel = logging.INFO if not '--debug' in argv else logging.DEBUG

logging.basicConfig(format='Texpoint2Texmaths - %(levelname)s: %(message)s', level=minlevel)

def latexfilter(global_magnification, imginfo):
    text = unicode(imginfo['source'])
    text = text.replace(r'\input{macros.sty}', '\usepackage{macros}')
    text = text.replace(r'\input{macros.txt}', '\usepackage{macros}')
    final_font_size = global_magnification * imginfo['font_size']
    return u"%.1f§latex§%s§svg§300§%s§txp_fig" % (final_font_size, text, 'TRUE' if imginfo['transparency'] else 'FALSE')



relationship_namespace = 'http://schemas.openxmlformats.org/package/2006/relationships'
presentation_namespace = "http://schemas.openxmlformats.org/presentationml/2006/main"
namespaces = {
    'p': presentation_namespace, 
    'r': "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
    'a': "http://schemas.openxmlformats.org/drawingml/2006/main"
}

namespaces_odp = {
    'draw': "urn:oasis:names:tc:opendocument:xmlns:drawing:1.0", 
    'style': "urn:oasis:names:tc:opendocument:xmlns:style:1.0", 
    'office': "urn:oasis:names:tc:opendocument:xmlns:office:1.0",
    'svg': "urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0",
    'meta': "urn:oasis:names:tc:opendocument:xmlns:meta:1.0",
    'text': "urn:oasis:names:tc:opendocument:xmlns:text:1.0",
    'presentation': "urn:oasis:names:tc:opendocument:xmlns:presentation:1.0",
    'xlink': "http://www.w3.org/1999/xlink"
}

try:
    basepath = join(argv[1], 'ppt', 'slides')
    odpdir = odpdir = argv[2]
except IndexError, e:
    print "Usage: %s pptxdir odpfile"
    print "pptxdir is the directory where the extracted (!) PPTX file is."
    print "odpdir is the directory where the extracted (!) ODP file is."
    exit(-1)
    
files = listdir(basepath)

warnings = []

# First, read the PPTX file to get the Pictures and their associated LaTeX-Code
logging.info("Step 1/5: Read PPTX file")
latex_codes = {}
slides = []
previous_font_size = 10 # is used only if the first texpoint displays have no BOXFONT attributes
for f in files:
    if not f.endswith('.xml'):
        continue
    # Assuming filename like 'slide42.xml'
    assert f.startswith('slide')
    slide = int(f.split('.')[0][5:])
    logging.debug("Processing slide number %d" % slide)
    slides.append(slide)
    doc = etree.parse(join(basepath, f))
    doc_rels = etree.parse(join(basepath, '_rels', "%s.rels" % f))
    list_of_pictures = doc.xpath('//p:cNvPr', namespaces=namespaces)
    for pic in list_of_pictures:
        name = pic.get('name')
        tags = pic.getparent().find('p:nvPr/p:custDataLst/p:tags', namespaces=namespaces)
        
        if tags is None:
            logging.debug("     Has zero tags, skipping!")
            continue
        rId = tags.get("{%s}id" % namespaces['r'])
        
        # Now get the tag*.xml filename from the rels document
        target = doc_rels.find('{%s}Relationship[@Id="%s"]' % (relationship_namespace, rId)).get('Target')
        if target is None or len(target) == 0:
            logging.debug("     Has no target, skipping!")
            continue

        # Now open the tag file to read the LaTeX code
        doc_tag = etree.parse(join(basepath, target))

        imginfo = {
            'source' : None,
            'picture_size' : None,
            'font_size' : previous_font_size,
            'transparency' : True
        }

        for tag in doc_tag.findall('p:tag', namespaces=namespaces):
            tagname = tag.get('name')
            val = tag.get('val')
            if tagname == 'SOURCE':
                imginfo['source'] = val
            elif tagname == 'BOXFONT':
                imginfo['font_size'] = float(val)
            elif tagname == 'PICTUREFILESIZE':
                imginfo['picture_size'] = int(val)
            elif tagname == 'TRANSPARENT':
                t = (val.lower() in ["wahr", "1", "true"]) # F***ed up file format: "Wahr" and "1" stand for True, "Falsch" and "0" for False
                imginfo['transparency'] = t


        if imginfo['source'] is None:
            logging.warn("Could not find source code for texpoint equation; skipping!")
            continue

        imgid = "%d:%s" % (slide, name)
        if imgid in latex_codes:
            latex_codes[imgid].append(imginfo)
            logging.debug("     Registered %d:%s (#%d)" % (slide, name, len(latex_codes[imgid])))
        else:
            latex_codes[imgid]=[imginfo]
            logging.debug("     Registered %d:%s" % (slide, name))
        
logging.info("Step 2/5: Read default global equation magnification")
# We now read the first tag, which contains the global texpoint information
global_tag = etree.parse(join(basepath, '..', 'tags', 'tag1.xml'))
try:
    # the DEFAULTMAGNIFICATION may be saved with a comma!
    mag_string = global_tag.find('p:tag[@name="DEFAULTMAGNIFICATION"]',
                                 namespaces=namespaces).get('val')
    global_magnification = float(mag_string.replace(',', '.'))
except AttributeError:
    logging.info("Could not find global magnification; using default (2.0)")
    global_magnification = 2.0


# Now read the ODP file and find out which LaTeX code belongs where.
# Reads from odpinputfile and writes to odpoutputfile

logging.info("Step 3/5: Write to ODP file (Meta Tags)")

metafile = join(odpdir, 'meta.xml')
doc = etree.parse(metafile)
previous_slide = None

# First, add meta tags
office_meta = doc.find('//office:meta', namespaces = namespaces_odp)
texmaths_metatag_preamble = etree.SubElement(office_meta, '{%s}user-defined' % namespaces_odp['meta'], {
    '{%s}name' % namespaces_odp['meta']: 'TexMathsPreamble'
})
texmaths_metatag_ignore_preamble = etree.SubElement(office_meta, '{%s}user-defined' % namespaces_odp['meta'], {
    '{%s}name' % namespaces_odp['meta']: 'TexMathsIgnorePreamble'
})
texmaths_metatag_ignore_preamble.text = "TRUE"
doc.write(metafile, xml_declaration = True)

contentfile = join(odpdir, 'content.xml')
doc = etree.parse(contentfile)
logging.info("Step 4/5: Write to ODP file (Equations)")
# Now, loop through frames and add attributes
frames = doc.findall('//draw:frame', namespaces = namespaces_odp)
for f in frames:
    name = f.get('{%s}name' % namespaces_odp['draw'])

    if not name or not name.startswith('Picture '):
        continue

    # Extract page number
    parent_page = f.getparent()
    while parent_page.tag == "{%s}g" % namespaces_odp['draw'] or parent_page.tag == "{%s}frame" % namespaces_odp['draw']: # walk through any grouped objects
        parent_page = parent_page.getparent()

    print parent_page.tag
    slide = int(parent_page.get('{%s}name' % namespaces_odp['draw'])[4:])
    if slide != previous_slide:
        logging.debug(" - Converting Slide %d..." % slide)
        previous_slide = slide

    if slide not in slides:
        raise Exception("Unknown slide: %d" % slide)

    imgid = "%d:%s" % (slide, name)
    if imgid not in latex_codes:
        logging.debug("     Could not find '%s' on slide %d" % (name, slide))
        continue

    # If we have seen only one image with that name on that slide, the case is easy
    if len(latex_codes[imgid]) == 1:
        imginfo = latex_codes[imgid][0]
    # But if we have more than one image with that name, we have to compare sizes
    else:
        logging.debug("     Calculcating size to determine correct match!")
        # Step 1: Get the byte size of the image in the odp file
        imagetag = f.find("draw:image", namespaces = namespaces_odp)

        length = getsize(join(odpdir, imagetag.get('{%s}href' % namespaces_odp['xlink'])))
        logging.debug("Length=%d, available lengths: %s" % (length, ', '.join((str(info['picture_size']) for info in latex_codes[imgid]))))
        # Step 2: Compare to candidates
        imginfo = min(latex_codes[imgid], key=lambda x:abs(x['picture_size']-length) if x['picture_size'] != None else 99999)
        logging.debug("Selected %d" % imginfo['picture_size'])
        if imginfo['picture_size'] != length:
            logging.warning("On slide %d, '%s' might have wrong source code." % (slide, name))



    logging.debug("    Found %s on slide %d!" % (name, slide))

    image_el = f.find('{%s}image' % namespaces_odp['draw'])

    latex_code = latexfilter(global_magnification, imginfo)

    new_title = etree.SubElement(image_el, '{%s}title' % namespaces_odp['svg'])
    new_title.text = u'TexMaths'
    new_desc = etree.SubElement(image_el, '{%s}desc' % namespaces_odp['svg'])
    new_desc.text = latex_code

logging.info("Step 5/5: Apply additional quirks")
logging.info(" - Remove page number duplicates")

# We remove a special attribute from the master-page page-number
# display.  From all non-master-pages, we remove the page-number
# display completely (but only if the page number was on the master
# page)
attribclass = '{%s}class' % namespaces_odp['presentation']
tagmasterpage = '{%s}master-page' % namespaces_odp['style']
onmasterpage = False
for pnumtag in doc.findall('//draw:frame', namespaces = namespaces_odp):
    if pnumtag.get(attribclass) == 'page-number':
        if pnumtag.getparent().tag == tagmasterpage:
            del pnumtag.attrib[attribclass]
            onmasterpage = True
        elif onmasterpage:
            pnumtag.getparent().remove(pnumtag)

logging.info("Saving...")
doc.write(contentfile, xml_declaration = True)

logging.info("Finished.")

