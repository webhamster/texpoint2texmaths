Texpoint2Texmaths
=================


Converts Powerpoint slides in PPT format to LibreOffice/OpenOffice
slides in ODP format while preserving any LaTeX equations in the
slides that have been created with Texpoint. The equations are
translated into the TexMaths format.

Note 1: This is early alpha stage. It works somewhat, but I give no
guarantees for anything.

Note 2: The converted slides only work with a TexMaths version that is
not released yet.

Installation
============

Copy both files to a location that is convenient for you, e.g.,
/usr/local/bin or ~/bin/. Then run tp2tm slidename.ppt. The output is
written to slidename.odp (same directory as original file).

Prerequisites
=============

* Only tested on Linux
* LibreOffice (conversion works best with latest version 4.2) or
 OpenOffice (not tested, you will have to change libreoffice to
 openoffice in tp2tmp manually)
* Wine (for the Microsoft Office Compatibility Pack, which the script
will try to download and install into a clean wine environment).
* Python 2.7

